from django.urls import path
from . import views

urlpatterns = [
    # 查询省份数据: GET /areas/
    path('areas/',views.ProvinceAreasView.as_view()),
    path('areas/<int:parentid>/',views.SubAreasView.as_view()),
]